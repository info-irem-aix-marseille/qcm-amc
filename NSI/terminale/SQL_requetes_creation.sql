CREATE TABLE annuaire (
    nom VARCHAR(100) NOT NULL,
    prenom VARCHAR(100) NOT NULL,
    tel VARCHAR(20) PRIMARY KEY);

CREATE TABLE annuaire (
    nom TEXT NOT NULL,
    prenom TEXT NOT NULL,
    tel INTEGER PRIMARY KEY);    

CREATE TABLE joueur (
    joueur_id INT PRIMARY KEY,
    nom VARCHAR(100) NOT NULL);

CREATE TABLE partie(
    j1 INT REFERENCES joueur(joueur_id),
    j2 INT REFERENCES joueur(joueur_id),
    score1 INT NOT NULL,
    score2 INT NOT NULL,
    CHECK (j1<>j2));

CREATE TABLE annuaire (
    nom TEXT NOT NULL,
    prenom TEXT NOT NULL,
    tel TEXT NOT NULL);  

CREATE TABLE annuaire (
    nom TEXT NOT NULL,
    prenom TEXT NOT NULL,
    tel TEXT PRIMARY KEY);  