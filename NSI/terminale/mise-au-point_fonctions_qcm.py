def f1(a, b):
    if a % b != 0 :
        return (a, b)
    else :
        return (b, a)

def f2(a, b):
    for l in a :
        if l ==  b :
            return True
    return False

"""
Renvoie le n-ième élément du plus long des deux tableaux d'entiers passés en paramètres
Entrée :
    * tableaux (???): un tuple de deux tableaux d'entiers
    * n (???): un nombre d'entier
Sortie :
    ??? 
"""

"""
Affiche les clés d'un dictionnaire pour lesquelles la valeur associée correspond au paramètre
Entrée :
    * dico (???) : un dictionnaire dont les clés sont des chaines de caractères et les valeurs 
    associée sont des tuples de deux entiers
    * couple (???) : le tuple recherché
Sortie :
    ???
"""