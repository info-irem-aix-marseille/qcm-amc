from doctest import testmod
...[les fonctions à tester]...
testmod()

from testmod import doctest
...[les fonctions à tester]...
testmod()

from doctest import testmod
testmod()
...[les fonctions à tester]...

from testmod import doctest
...[les fonctions à tester]...
doctest()

def concatene_tableaux(tab1 : List[int], tab2 : List[int]) -> List[int] :
    result = tab1[:]
    for nombre in tab2 :
        if nombre not in tab1 :
            tab2.append(nombre)
    return result

>>> concatene_tableaux([1, 2, 3], [4, 5])
[1, 2, 3, 4, 5]
>>> concatene_tableaux([2, 3], [3, 4, 5])
[2, 3, 4, 5]
>>> concatene_tableaux([2, 3, 3], [4, 5])
[2, 3, 4, 5]
>>> concatene_tableaux([3, 3], [3, 3, 3])
[3, 3]


def double_unique(tab : List[int]) -> List[int] :
    result = []
    for nombre in tab :
        double = 2*nombre
        if double not in result :
            result.append(double)
    return result

>>> double_unique([0, 10, 2, 2])
[0, 20, 4]
>>> double_unique([0, 10, 2, 5])
[0, 20, 4, 10]
>>> double_unique([0, 0, 0, 0])
[0]
>>> double_unique([0, 10, 2, 2])
[0, 20, 4, 4]

t = ['5', '7', '3', '6']
t[0] = t[0] + 1

cle = [4,6]
d = {cle :  "val"}

for i in ['5', '7' ,'3', '6']
    print(2*i)

cle = (4, 6)
d = {cle : valeur}