def myst(lst : Cell, v: int) -> ?:
    "lst est un objet de type Cell"
    if lst is None :
        return False
    if lst.valeur == v :
        return True
    return myst(lst.suivante, v)

def nombre_occurence(tab : list[int], n : int) -> int :
    occ = 0
    for valeur in tab :
        if valeur == n :
            occ += 1
    return occ
