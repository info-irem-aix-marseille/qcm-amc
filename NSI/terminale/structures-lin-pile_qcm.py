class Personnage:
    def __init__(self,t,p,e):
        self.type = t
        self.points = p
        self.energie = e

class Cellule :
    def __init__(self, v, s):
        self.valeur = v
        self.suivante = s

class Pile :
    def __init__(self):
        self.tete = None

    def empiler(self, v):
        ...

class Cell :
    def __init__(self, v, s):
        self.valeur = v
        self.suivante = s

class Pile :
    def __init__(self):
        self.contenu = None

    def empiler(self, v):
        ...
