## Intentions

Ce dépôt vise à constituer une banque de questions à destination de l'enseignement de la NSI et de la SNT, utilisables avec le logiciel [Auto Multiple Choice](https://www.auto-multiple-choice.net/index.fr)(AMC). 

Un des (nombreux) intérêts d'AMC est qu'il permet d'introduire une part plus ou moins perfectionnée
d'aléatoire dans la composition des sujets, faisant de lui un outil idéal pour un 
entrainement régulier des élèves.

## Langages

Les questions sont écrites en $\LaTeX$, mais certains codes en Python ou SQL sont contenus dans des fichiers annexes.

## Présentation des questions

Les fichiers `*_questions.tex` contiennent uniquement des questions, classées par 
thématiques. À titre d'exemple, certains modèles d'utilisations de ces questions sont 
proposés, incluant notamment les commandes basées sur le package `minted`.

## Export vers Moodle

Selon la typologie des questions, il est possible de les exporter vers un format
compatible avec Moodle.

